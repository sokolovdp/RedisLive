#! /usr/bin/env python

import redis
import datetime
import threading
import traceback
import argparse
import time
from threading import Timer

from src.api.util import settings
from src.dataprovider.dataprovider import RedisLiveDataProvider


class Monitor:
    """Monitors a given Redis server using the MONITOR command.
    """

    def __init__(self, connection_pool):
        """Initializes the Monitor class.

        Args:
            connection_pool (redis.ConnectionPool): Connection pool for the \
                    Redis server to monitor.
        """
        self.connection_pool = connection_pool
        self.connection = None

    def __del__(self):
        try:
            self.reset()
        except Exception:
            pass

    def reset(self):
        """If we have a connection, release it back to the connection pool.
        """
        if self.connection:
            self.connection_pool.release(self.connection)
            self.connection = None

    def monitor(self):
        """Kicks off the monitoring process and returns a generator to read the
        response stream.
        """
        if self.connection is None:
            self.connection = self.connection_pool.get_connection('monitor', None)
        self.connection.send_command("monitor")
        return self.listen()

    def parse_response(self):
        """Parses the most recent responses from the current connection.
        """
        return self.connection.read_response()

    def listen(self):
        """A generator which yields responses from the MONITOR command.
        """
        while True:
            yield self.parse_response()


def error_handler(trace_back, command):
    print("\n==============================\n")
    print(datetime.datetime.now())
    print(trace_back)
    print(command)
    print("==============================\n")


class MonitorThread(threading.Thread):
    """Runs a thread to execute the MONITOR command against a given Redis server
    and store the resulting aggregated statistics in the configured stats
    provider.
    """

    def __init__(self, server, port, password=None):
        """Initializes a MonitorThread.

        Args:
            server (str): The host name or IP of the Redis server to monitor.
            port (int): The port to contact the Redis server on.

        Kwargs:
            password (str): The password to access the Redis host. Default: None
        """
        super(MonitorThread, self).__init__()
        self.server = server
        self.port = port
        self.password = password
        self.id = self.server + ":" + str(self.port)
        self._stop = threading.Event()

    def stop(self):
        """Stops the thread.
        """
        self._stop.set()

    def stopped(self):
        """Returns True if the thread is stopped, False otherwise.
        """
        return self._stop.is_set()

    def run(self):
        """Runs the thread.
        """
        stats_provider = RedisLiveDataProvider.get_provider()
        pool = redis.ConnectionPool(host=self.server, port=self.port, db=0,
                                    password=self.password)
        redis_monitor = Monitor(pool)
        bytes_input = redis_monitor.monitor()

        for bytes_command in bytes_input:
            command = bytes_command.decode()
            try:
                parts = command.split(" ")

                if len(parts) == 1:
                    continue

                epoch = float(parts[0].strip())
                timestamp = datetime.datetime.fromtimestamp(epoch)

                # Strip '(db N)' and '[N x.x.x.x:xx]' out of the monitor str
                if (parts[1] == "(db") or (parts[1][0] == "["):
                    parts = [parts[0]] + parts[3:]

                command = parts[1].replace('"', '').upper()

                if len(parts) > 2:
                    keyname = parts[2].replace('"', '').strip()
                else:
                    keyname = None

                if not command == 'INFO' and not command == 'MONITOR':
                    stats_provider.save_monitor_command(self.id,
                                                        timestamp,
                                                        command,
                                                        keyname)
            except Exception:
                error_handler(traceback.format_exc(), command)

            if self.stopped():
                break


class InfoThread(threading.Thread):
    """Runs a thread to execute the INFO command against a given Redis server
    and store the resulting statistics in the configured stats provider.
    """

    def __init__(self, server, port, password=None):
        """Initializes an InfoThread instance.

        Args:
            server (str): The host name of IP of the Redis server to monitor.
            port (int): The port number of the Redis server to monitor.

        Kwargs:
            password (str): The password to access the Redis server. \
                    Default: None
        """
        threading.Thread.__init__(self)
        self.server = server
        self.port = port
        self.password = password
        self.id = self.server + ":" + str(self.port)
        self._stop = threading.Event()

    def stop(self):
        """Stops the thread.
        """
        self._stop.set()

    def stopped(self):
        """Returns True if the thread is stopped, False otherwise.
        """
        return self._stop.is_set()

    def run(self):
        """Does all the work.
        """
        stats_provider = RedisLiveDataProvider.get_provider()
        redis_client = redis.StrictRedis(host=self.server, port=self.port, db=0, password=self.password)

        while not self.stopped():
            try:
                redis_info = redis_client.info()
                current_time = datetime.datetime.now()
                used_memory = int(redis_info['used_memory'])

                try:
                    peak_memory = int(redis_info['used_memory_peak'])
                except Exception:  # used_memory_peak not available in older versions of redis
                    peak_memory = used_memory

                stats_provider.save_memory_info(self.id, current_time, used_memory, peak_memory)
                stats_provider.save_info_command(self.id, current_time, redis_info)

                time.sleep(1)

            except Exception:
                error_handler(traceback.format_exc(), " InfoThread error !")


class RedisMonitor:

    def __init__(self):
        self.threads = []
        self.active = True

    def run(self, monitor_duration):
        """Monitors all redis servers defined in the config for a certain number
        of seconds.

        Args:
            monitor_duration (int): The number of seconds to monitor for.
        """
        redis_servers = settings.get_redis_servers()

        for redis_server in redis_servers:
            redis_password = redis_server.get("password")

            redis_monitor = MonitorThread(redis_server["server"], redis_server["port"], redis_password)
            redis_monitor.setDaemon(True)
            self.threads.append(redis_monitor)
            redis_monitor.start()

            info = InfoThread(redis_server["server"], redis_server["port"], redis_password)
            info.setDaemon(True)
            self.threads.append(info)
            info.start()

        run_timer = Timer(monitor_duration, self.stop)
        run_timer.start()

        try:
            while self.active:
                pass
        except (KeyboardInterrupt, SystemExit):
            self.stop()
            run_timer.cancel()

    def stop(self):
        """Stops the monitor and all associated threads.
        """
        if not args.quiet:
            print("shutting down redis monitor...")
        for thread in self.threads:
            thread.stop()
        self.active = False


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Monitor redis.')
    parser.add_argument('--duration',
                        type=int,
                        help="duration to run the monitor command (in seconds)",
                        required=True)
    parser.add_argument('--quiet',
                        help="do not write anything to standard output",
                        required=False,
                        action='store_true')
    args = parser.parse_args()
    duration = args.duration
    monitor = RedisMonitor()
    monitor.run(duration)
